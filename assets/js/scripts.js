$(document).ready(function(e){
	
	var next = $('.button-next-slider').attr('data-next');
	var prev = $('.button-prev-slider').attr('data-prev');
	
	$('.slider .dots a').click(function(){
		
		$('.slider .dots a, .slider .item-slider').removeClass('active');
		
		var dot = $(this).addClass('active').attr('data-dot');
		
		var prev = (dot--) - 1;
		var next = dot++;
		
		prev = $('.button-prev-slider[data-prev="'+prev+'"]');
		next = $('.button-next-slider[data-next="'+next+'"]');
		
		$('.slider .item-slider[data-item="'+ dot +'"]').addClass('active');
		
		return false;
		
	});
	
	console.log(next);
	
	$('.button-next-slider').click(function(){
		next++;
		$('.slider .dots a, .slider .item-slider').removeClass('active');
		$('.slider .dots a[data-dot="'+ next +'"], .slider .item-slider[data-item="'+ next +'"]').addClass('active');
		return false;
	});
	
	$('.button-prev-slider').click(function(){
		next--;
		$('.slider .dots a, .slider .item-slider').removeClass('active');
		$('.slider .dots a[data-dot="'+ next +'"], .slider .item-slider[data-item="'+ next +'"]').addClass('active');
		return false;
	});
	
	console.log(e);
	
});